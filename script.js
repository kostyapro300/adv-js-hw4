// HW ADVANCHED JS

/* 
AJAX -  технологія, яка дозволяє взаємодіяти з сервером без перезавантаження сторінки веб-сайту
За допомогою AJAX, веб сторінки можуть асинхронно відправляти та отримувати дані
з сервера без перезавантаженя
  */ 

// для меня эта тема сложновата, по этому буду описывать свои каждые шаги, что бы лучше запомнить. 

// анимка загрузки
const loadingElement = document.getElementById('loading');
if (loadingElement) {
  charactersContainer.removeChild(loadingElement);
}

//  Создаем штуку, которая дозволить нам взаимодействовать с сервером 
const xhr = new XMLHttpRequest();

// Відкриття запиту 

xhr.open('GET', 'https://ajax.test-danit.com/api/swapi/films', true);

// Обрабатываем и отлеживаем стан запиту 

xhr.onreadystatechange = function() { // onreadystatechange буде викликаний кожного разу, коли стан обьєкта реквеста буде змінюватися
    if (xhr.readyState === XMLHttpRequest.DONE) { // когда реквест DONE, проверяем статус ответа 
        if (xhr.status === 200 ) { 
            const filmsData = JSON.parse(xhr.responseText);
            displayFilms(filmsData)
        } else {
            console.error('Ошибка отримання даних про фільми ');
        }
    }
}

xhr.send(); 

// Отображаем списки фильмолв 

function displayFilms(filmsData) {
    const filmsList = document.getElementById('films-list')

    filmsData.forEach(film => {
        const listItem = document.createElement('li')
        listItem.textContent = `Эпизод ${film.episodeId}: ${film.name} - ${film.openingCrawl}`
        filmsList.appendChild(listItem)
    });
}

// Отримаэмо список персонажей для кожного фильму 

function displayCharacters(charactersList, filmTitle) {
    const charactersContainer = document.getElementById('characters-container');
    const filmCharactersList = document.createElement('ul');
    const filmTitleElement = document.createElement('h2');
    filmTitleElement.textContent = filmTitle;

    charactersList.forEach( character => {
        const characterItem = document.createElement('li')
        characterItem.textContent = character.name 
        filmCharactersList.appendChild(characterItem)
    });

    // типо выводим "загрузку"
    const loadingElement = document.getElementById('loading');
    if (loadingElement) {
        charactersContainer.removeChild(loadingElement);
    }

    charactersContainer.appendChild(filmTitleElement);
    charactersContainer.appendChild(filmCharactersList);
}

